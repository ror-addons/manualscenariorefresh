ScenarioRefresh = {}
ScenarioRefresh.ScenarioRefreshShowHook = nil
ScenarioRefresh.ScenarioRefreshHideHook = nil

function ScenarioRefresh.Initialize()
	WindowUnregisterEventHandler("ScenarioSummaryWindow",SystemData.Events.SCENARIO_PLAYERS_LIST_STATS_UPDATED)
	CreateWindow ("ScenarioRefreshButton", false)
	WindowAddAnchor ("ScenarioRefreshButton","bottom", "ScenarioSummaryWindowClockImage", "top", 40,20)
	ButtonSetText ("ScenarioRefreshButton", L"Refresh")
	ScenarioRefresh.ScenarioRefreshShowHook = ScenarioSummaryWindow.OnShown
	ScenarioRefresh.ScenarioRefreshHideHook = ScenarioSummaryWindow.OnHidden
	ScenarioSummaryWindow.OnShown = ScenarioRefresh.OnShown
	ScenarioSummaryWindow.OnHidden = ScenarioRefresh.OnHidden
end

function ScenarioRefresh.Refresh() 
	ScenarioSummaryWindow.OnPlayerListStatsUpdated()
end

function ScenarioRefresh.OnShown()
	ScenarioRefresh.ScenarioRefreshShowHook()
	WindowSetShowing("ScenarioRefreshButton", true)
end

function ScenarioRefresh.OnHidden()
	ScenarioRefresh.ScenarioRefreshHideHook()
	WindowSetShowing("ScenarioRefreshButton", false)
end